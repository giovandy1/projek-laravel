<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }
    public function submit(Request $request)
    {
        $namefirst = $request['namefirst'];
        $namelast = $request['namelast'];
        return view('page.welcome', compact('namefirst', 'namelast'));
    }
}
